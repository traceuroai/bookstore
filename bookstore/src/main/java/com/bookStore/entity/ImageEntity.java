package com.bookStore.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "images")
@Data
public class ImageEntity extends AbstractEntity{
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "url")
	private String url;
	
	// image - book
	@ManyToMany(mappedBy = "imageBook")
	private List<BookEntity> bookImage = new ArrayList<>();
	
	//image - blog
	@ManyToMany(mappedBy = "imageBlog")
	private List<BlogEntity> blogImage = new ArrayList<>();
	
	//image - user
	@ManyToMany(mappedBy = "imageUser")
	private List<UserEntity> userImage = new ArrayList<>();

}
// table image của user, blog, book sẽ tự sinh (spring data jpa)