package com.bookStore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "comment")
@Data
public class CommentEntity extends AbstractEntity{
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "likes")
	private int likes;
	
	// comment book
	@ManyToOne
	@JoinColumn(name = "idbook")
	private BookEntity bookComment;
	
	// comment User
	@ManyToOne
	@JoinColumn(name = "iduser")
	private UserEntity userComment;
	
}
