package com.bookStore.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "bookcart")
@Data
public class BookCartEntity extends AbstractEntity{
	
	@Column(name = "status")
	private String status;
	
	// bookCart - User
	@OneToOne
    @JoinColumn(name = "iduser")
	private UserEntity user;
	
	// bookCart - Orders
	@OneToMany(mappedBy = "bookCart")
	private List<OrderEntity> orders = new ArrayList<>();
	
	// bookCart - bookCartDetail
	@OneToOne(mappedBy = "bookCart", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private BookCartDetailEntity bookCartDetail;
	
}
