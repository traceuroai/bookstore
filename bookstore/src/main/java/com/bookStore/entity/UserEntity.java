package com.bookStore.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "user")
@Data
public class UserEntity extends AbstractEntity{

	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "fullname")
	private String fullName;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "sex")
	private String sex;
	
	@Column(name = "dateofbirthd")
	private Date dateOfBirthd;
	
	@Column(name = "status")
	private String status;
	
	
	/*
	 * Chú ý khi sử dụng @ManyToMany : @JoinTable : table trung gian (name = "")
	 * joinColumn thứ nhất sẽ là id của table đang đứng
	 * inverseJoinColumns sẽ là id của bảng cần liên kết
	 * */
	@ManyToMany
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "id_user"), inverseJoinColumns = @JoinColumn(name = "id_role"))
	private List<RoleEntity> roles = new ArrayList<>();
	
	// user - comment
	@OneToMany(mappedBy = "userComment")
	private List<CommentEntity> comments = new ArrayList<>();
	
//	// user - bookCart
//	@OneToMany(mappedBy = "user")
//	private List<BookCartEntity> bookCarts = new ArrayList<>();
	
	// user - image
	@ManyToMany
	@JoinTable(name = "image_user", joinColumns = @JoinColumn(name = "id_user"), inverseJoinColumns = @JoinColumn(name = "id_image"))
	private List<ImageEntity> imageUser = new ArrayList<>();
	
	// user - order 
	@OneToMany(mappedBy = "user")
	private List<OrderEntity> orders = new ArrayList<>();
	
}
