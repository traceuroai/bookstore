package com.bookStore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "bookcartdetail")
@Data
public class BookCartDetailEntity{
	
	@Id
	private Long id;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "price")
	private Double price;

	// bookCart - bookCartDetail
	@OneToOne
    @MapsId
    @JoinColumn(name = "idbookcart")
    private BookCartEntity bookCart;
	
	// book - bookCartDetail
	@ManyToOne
	@JoinColumn(name = "idbook")
	private BookEntity book;

}
