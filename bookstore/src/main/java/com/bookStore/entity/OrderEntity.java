package com.bookStore.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "orders")
@Data
public class OrderEntity extends AbstractEntity{
	
	@Column(name = "shipAddress")
	private String shipAddress;// địa chỉ giao hàng
	
	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "price")
	private Double price;
	
	@Column(name = "totalmoney")
	private Double totalMoney;
	
	@Column(name = "payMethod")
	private String payMethod;//phương thức thanh toán
	
	@Column(name = "status")
	private String status;
	
	// order - user
	@ManyToOne
	@JoinColumn(name = "iduser")
	private UserEntity user;

	// order - orderdetail
	@OneToOne(mappedBy = "orders", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private OrderDetailEntity orderDetail;
	
	// order - book cart
	@ManyToOne
	@JoinColumn(name = "idbookcart")
	private BookCartEntity bookCart;
	
}
