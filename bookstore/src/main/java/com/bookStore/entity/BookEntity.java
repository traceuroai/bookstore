package com.bookStore.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "book")
@Data
public class BookEntity extends AbstractEntity{
	
	@Column(name = "booktitle")
	private String bookTitle;
	
	@Column(name = "shortdescription")
	private String shortDescription;
	
	@Column(name = "author")
	private String author;// tác giả
	
	@Column(name = "price")
	private Double price;
	
	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "evaluate")
	private int evaluate;// đánh giá
	
	@Column(name = "status")
	private String status;
	
	// thể loại
	@ManyToOne
	@JoinColumn(name = "idcategory")// tên cột
	private CategoryEntity category;
	
	// chi tiết sách
	@OneToOne(mappedBy = "book", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private BookDetailEntity bookdetail;
	
	// book - Comment
	@OneToMany(mappedBy = "bookComment")
	List<CommentEntity> comments = new ArrayList<>();
	
	// Book - BookCartDetail
	@OneToMany(mappedBy = "book")
	List<BookCartDetailEntity> bookCartDetail = new ArrayList<>();
	
	// book - image
	@ManyToMany
	@JoinTable(name = "image_book", joinColumns = @JoinColumn(name = "id_book"), inverseJoinColumns = @JoinColumn(name = "id_image"))
	private List<ImageEntity> imageBook = new ArrayList<>();
	
}
