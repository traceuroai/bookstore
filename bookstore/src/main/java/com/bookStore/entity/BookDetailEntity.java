package com.bookStore.entity;

import java.time.Year;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "bookdetail")
@Data
public class BookDetailEntity{
	
	@Id
	private Long id;
	
	@Column(name = "publisher")
	private String publisher;// nhà xuất bản
	
	@Column(name = "publishingyear")
	private Year publishingYear;// năm xuất bản
	
	@Column(name = "pagenumber")
	private int pageNumber;// số trang
	
	@Column(name = "form")
	private String form;// hình thức
	
	@Column(name = "language")
	private String language;
	
	@OneToOne
    @MapsId
    @JoinColumn(name = "idbook")
    private BookEntity book;
	
	
}
