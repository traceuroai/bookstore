package com.bookStore.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "orderdetail")
@Data
public class OrderDetailEntity{
	
	@Id
	private Long id;
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "carrier")
	private String carrier;// vận chuyển (người hoặc đơn vị)
	
	@OneToOne
    @MapsId
    @JoinColumn(name = "idorder")
    private OrderEntity orders;
}
