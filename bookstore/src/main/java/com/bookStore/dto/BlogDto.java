package com.bookStore.dto;

import lombok.Data;

@Data
public class BlogDto {

	private String title;
	private String content;
	private String views;
	
}
