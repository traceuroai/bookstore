package com.bookStore.dto;

import lombok.Data;

@Data
public class BookCartDto extends AbstractDto<BookCartDto>{
	
	private Long idUser;
	private Long idBook;
	private int quantity;
	private Double price;
	private String status;

}
