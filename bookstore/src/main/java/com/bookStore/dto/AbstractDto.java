package com.bookStore.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class AbstractDto<T> {
	
	private Long id;
	private Date createdDate;
	private Date modifiedDate;
	private String createdBy;
	private String modifiedBy;
//	private long[] ids;// không cần sử dụng như jsp-servlet (nhận vào 1 model rồi mới getIds)
	private List<T> listResult = new ArrayList<T>();
	private Integer page;
	private Integer limit;
	private Integer totalPage;
	private Integer totalItem;
	private String sortName;
	private String sortBy;
	private String alert;
	private String message;
	private String type;
	
}
