package com.bookStore.dto;

import java.time.Year;

import lombok.Data;

@Data
public class BookDto extends AbstractDto<BookDto>{

	private Long categoryId;
	private String bookTitle;
	private String shortDescription;
	private String author;// tác giả
	private Double price;
	private int quantity;
	private int evaluate;// đánh giá
	private String status;
	private String publisher;// nhà xuất bản
	private Year publishingYear;// năm xuất bản
	private int pageNumber;// số trang
	private String form;// hình thức
	private String language;
//	private int remaining;// còn lại (bổ xung thêm csdl)
	private String categoryCode;// mã thể loại
	
}
