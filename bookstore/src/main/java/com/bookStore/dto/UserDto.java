package com.bookStore.dto;

import java.util.Date;

import lombok.Data;

@Data
public class UserDto extends AbstractDto<UserDto>{
	
	private String email;
	private String fullName;
	private String password;
	private String status;
	private String phone;
	private String address;
	private String sex;
	private Date dateOfBirth;
	private RoleDto role;


}
