package com.bookStore.dto;

import lombok.Data;

@Data
public class CategoryDto extends AbstractDto<CategoryDto>{
	
	private String code;
	private String name;

}
