package com.bookStore.dto;

import lombok.Data;

@Data
public class ImageDto extends AbstractDto<ImageDto>{

	private String name;
	private String url;
	
}
