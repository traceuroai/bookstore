package com.bookStore.dto;

import lombok.Data;

@Data
public class NewDto {

	private String title;
	private String content;
	
}
