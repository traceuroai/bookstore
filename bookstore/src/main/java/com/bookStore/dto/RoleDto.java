package com.bookStore.dto;

import lombok.Data;

@Data
public class RoleDto extends AbstractDto<RoleDto>{
	
	private String code;
	private String name;

}
