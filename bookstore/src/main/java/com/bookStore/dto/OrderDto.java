package com.bookStore.dto;

import lombok.Data;

@Data
public class OrderDto extends AbstractDto<OrderDto>{

	private Long idUser;
	private Long idBookCart;
	private String shipAddress;// địa chỉ giao hàng
	private int quantity;
	private Double price;
	private Double totalMoney;
	private String payMethod;//phương thức thanh toán
	private String status;
	private String note;
	private String carrier;// vận chuyển (người hoặc đơn vị)
	
}
