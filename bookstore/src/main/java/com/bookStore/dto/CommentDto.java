package com.bookStore.dto;

import lombok.Data;

@Data
public class CommentDto extends AbstractDto<CommentDto>{

	private Long idUser;
	private Long idBook;
	private String content;
	private String status;
	private int likes;
	
}
