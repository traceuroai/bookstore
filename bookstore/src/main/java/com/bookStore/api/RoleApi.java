package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.RoleDto;
import com.bookStore.service.IRoleService;

@RestController
public class RoleApi {
	
	@Autowired
	private IRoleService roleService;
	
	@PostMapping (value = "/role")
	public RoleDto createRole (@RequestBody RoleDto roleDto)
	{
		return null;
	}
	
	@PutMapping (value = "/role")
	public RoleDto updateRole (@RequestBody RoleDto roleDto)
	{
		return null;
	}
	
	@DeleteMapping(value = "/role")
	public void deleteRole (@RequestBody RoleDto roleDto)
	{
		
	}

}
