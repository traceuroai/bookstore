package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.BlogDto;
import com.bookStore.service.IBlogService;

@RestController
public class BlogApi {

	@Autowired
	private IBlogService blogService;
	
	@PostMapping(value = "/blog")
	public BlogDto createBlog(@RequestBody BlogDto blogDto)
	{
		return null;
	}
	
	@PutMapping(value = "/blog")
	public BlogDto updateBlog(@RequestBody BlogDto blogDto)
	{
		return null;
	}
	
	@DeleteMapping(value = "/blog")
	public void deleteBlog(@RequestBody long [] ids)
	{
		
	}
	
}
