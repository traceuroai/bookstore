package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.CommentDto;
import com.bookStore.service.ICommentService;

@RestController
public class CommentApi {
	
	@Autowired
	private ICommentService commentService;
	
	@PostMapping(value = "/comment")
	public CommentDto createComment(@RequestBody CommentDto commentDto)
	{
		return null;
	}
	
	@PutMapping(value = "/comment")
	public CommentDto updateComment(@RequestBody CommentDto commentDto)
	{
		return null;
	}
	
	@DeleteMapping(value = "/comment")
	public void deleteComment(@RequestBody long [] ids)
	{
		
	}

}
