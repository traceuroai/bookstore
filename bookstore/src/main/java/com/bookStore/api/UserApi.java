package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.UserDto;
import com.bookStore.service.IUserService;

@RestController
public class UserApi {
	
	@Autowired
	private IUserService userService;
	
	@PostMapping(value = "/user")
	public UserDto createUser(@RequestBody UserDto userDto)
	{
		return null;
	}
	
	@PutMapping(value = "/user")
	public UserDto updateUser(@RequestBody UserDto userDto)
	{
		return null;
	}
	
	@DeleteMapping(value = "/user")
	public void deleteUser (@RequestBody long [] ids)
	{
		
	}

}
