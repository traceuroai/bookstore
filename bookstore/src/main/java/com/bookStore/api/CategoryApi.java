package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.CategoryDto;
import com.bookStore.service.ICategoryService;

@RestController
public class CategoryApi {
	
	@Autowired 
	private ICategoryService categoryService;
	
	@PostMapping(value = "/category")
	public CategoryDto createCategory(@RequestBody CategoryDto categoryDto)
	{
		return null;
	}
	
	@PutMapping(value = "/category")
	public CategoryDto updateCategory(@RequestBody CategoryDto categoryDto)
	{
		return null;
	}
	
	@DeleteMapping(value = "/category")
	public void deleteCategory(@RequestBody long [] ids)
	{
		
	}

}
