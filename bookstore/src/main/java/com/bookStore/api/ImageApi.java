package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.ImageDto;
import com.bookStore.service.IImageService;

@RestController
public class ImageApi {
	
	@Autowired
	private IImageService imageService;
	
	@PostMapping(value = "/image")
	public ImageDto createImage(@RequestBody ImageDto imageDto)
	{
		return null;
	}
	
	@PutMapping(value = "/image")
	public ImageDto updateImage(@RequestBody ImageDto imageDto)
	{
		return null;
	}
	
	@DeleteMapping(value = "/image")
	public void deleteImage(@RequestBody ImageDto imageDto)
	{
		
	}

}
