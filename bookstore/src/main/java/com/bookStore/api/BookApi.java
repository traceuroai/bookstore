package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.BookDto;
import com.bookStore.service.IBookService;

@RestController
public class BookApi {
	
	@Autowired
	private IBookService bookService;

	@PostMapping(value = "/book")
	public BookDto createBook (@RequestBody BookDto bookdto)
	{
		return bookService.save(bookdto);
	}
	
	@PutMapping(value = "/book/{id}")
	public BookDto updateBook(@RequestBody BookDto bookdto, @PathVariable("id") Long id)
	{
		bookdto.setId(id);
		return bookService.update(bookdto);
	}
	
	@DeleteMapping(value = "/book")
	public void deleteBook(@RequestBody long[] ids)
	{
		bookService.delete(ids);
	}
	
}
