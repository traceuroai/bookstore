package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.BookCartDto;
import com.bookStore.service.IBookCartService;

@RestController
public class BookCartApi {
	
	@Autowired
	private IBookCartService bookCartSerice;
	
	@PostMapping(value = "/bookcart")
	public BookCartDto createBookCart(@RequestBody BookCartDto bookcartDto)
	{
		return null;
	}
	
	@PutMapping(value = "/bookcart")
	public BookCartDto updateBookCart(@RequestBody BookCartDto bookcartDto)
	{
		return null;
	}
	
	@DeleteMapping(value = "/bookcart")
	public void deleteBookCart(@RequestBody long [] ids)
	{
		
	}

}
