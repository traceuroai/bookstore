package com.bookStore.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookStore.dto.OrderDto;
import com.bookStore.service.IOrderService;

@RestController
public class OrderApi {
	
	@Autowired
	private IOrderService orderService;
	
	@PostMapping(value = "/order")
	public OrderDto createOrder (@RequestBody OrderDto orderDto)
	{
		return null;
	}
	
	@PutMapping(value = "/order")
	public OrderDto updateOrder (@RequestBody OrderDto orderDto)
	{
		return null;
	}
	
	@DeleteMapping(value = "/order")
	public void deleteOrder(@RequestBody long [] ids )
	{
		
	}

}
