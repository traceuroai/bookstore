package com.bookStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookStore.entity.CommentEntity;

public interface CommentRepository extends JpaRepository<CommentEntity, Long>{

}
