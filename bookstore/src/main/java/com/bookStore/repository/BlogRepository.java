package com.bookStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookStore.entity.BlogEntity;

public interface BlogRepository extends JpaRepository<BlogEntity, Long>{

}
