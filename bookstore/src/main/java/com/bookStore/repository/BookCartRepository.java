package com.bookStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookStore.entity.BookCartEntity;

public interface BookCartRepository extends JpaRepository<BookCartEntity, Long>{

}
