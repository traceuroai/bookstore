package com.bookStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookStore.entity.BookEntity;

public interface BookRepository extends JpaRepository<BookEntity, Long>{
	
	BookEntity findOneById(Long id);

}
