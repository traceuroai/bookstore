package com.bookStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookStore.entity.RoleEntity;

public interface RoleRepository extends JpaRepository<RoleEntity, Long>{

}
