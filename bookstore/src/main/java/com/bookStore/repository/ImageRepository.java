package com.bookStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookStore.entity.ImageEntity;

public interface ImageRepository extends JpaRepository<ImageEntity, Long>{

}
