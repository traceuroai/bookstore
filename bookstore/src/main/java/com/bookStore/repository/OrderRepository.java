package com.bookStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookStore.entity.OrderEntity;

public interface OrderRepository extends JpaRepository<OrderEntity, Long>{

}
