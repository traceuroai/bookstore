package com.bookStore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bookStore.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long>{

}
