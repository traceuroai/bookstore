package com.bookStore.converter;

import org.springframework.stereotype.Component;

import com.bookStore.dto.BookDto;
import com.bookStore.entity.BookEntity;

@Component
public class BookConverter {

	public BookEntity toEntity(BookDto bookDTO)
	{
		BookEntity bookEntity = new BookEntity();
		bookEntity.setBookTitle(bookDTO.getBookTitle());
		bookEntity.setShortDescription(bookDTO.getShortDescription());
		bookEntity.setAuthor(bookDTO.getAuthor());
		bookEntity.setPrice(bookDTO.getPrice());
		bookEntity.setQuantity(bookDTO.getQuantity());
		bookEntity.setEvaluate(bookDTO.getEvaluate());
		bookEntity.setStatus(bookDTO.getStatus());
		return bookEntity;
	}
	
	public BookDto toDto(BookEntity bookEntity)
	{
		BookDto bookDto = new BookDto();
		bookDto.setId(bookEntity.getId());
		bookDto.setBookTitle(bookEntity.getBookTitle());
		bookDto.setShortDescription(bookEntity.getShortDescription());
		bookDto.setAuthor(bookEntity.getAuthor());
		bookDto.setPrice(bookEntity.getPrice());
		bookDto.setQuantity(bookEntity.getQuantity());
		bookDto.setEvaluate(bookEntity.getEvaluate());
		bookDto.setStatus(bookEntity.getStatus());
		bookDto.setCategoryCode(bookEntity.getCategory().getCode());
		bookDto.setCreatedDate(bookEntity.getCreatedDate());
		bookDto.setCreatedBy(bookEntity.getCreatedBy());
		bookDto.setModifiedDate(bookEntity.getModifiedDate());
		bookDto.setModifiedBy(bookEntity.getModifiedBy());
		return bookDto;
	}
	
	public BookEntity toEntity(BookDto bookDTO, BookEntity oldBook)
	{
		oldBook.setBookTitle(bookDTO.getBookTitle());
		oldBook.setShortDescription(bookDTO.getShortDescription());
		oldBook.setAuthor(bookDTO.getAuthor());
		oldBook.setPrice(bookDTO.getPrice());
		oldBook.setQuantity(bookDTO.getQuantity());
		oldBook.setEvaluate(bookDTO.getEvaluate());
		oldBook.setStatus(bookDTO.getStatus());
		return oldBook;
	}
	
}
