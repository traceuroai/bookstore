package com.bookStore.service.implement;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookStore.converter.BookConverter;
import com.bookStore.dto.BookDto;
import com.bookStore.entity.BookEntity;
import com.bookStore.entity.CategoryEntity;
import com.bookStore.repository.BookRepository;
import com.bookStore.repository.CategoryRepository;
import com.bookStore.service.IBookService;

@Service
public class BookService implements IBookService{

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private BookConverter bookConverter;
	
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public BookDto save(BookDto bookDto) {
		CategoryEntity categoryEntity = categoryRepository.findOneByCode(bookDto.getCategoryCode());
		BookEntity bookEntity = bookConverter.toEntity(bookDto);
		bookEntity.setCategory(categoryEntity);
		bookEntity = bookRepository.save(bookEntity);
		return bookConverter.toDto(bookEntity);
	}

	@Override
	public BookDto update(BookDto bookDto) {
		BookEntity oldBook = bookRepository.findOneById(bookDto.getId());
		BookEntity bookEntity = bookConverter.toEntity(bookDto, oldBook);
		CategoryEntity categoryEntity = categoryRepository.findOneByCode(bookDto.getCategoryCode());
		bookEntity.setCategory(categoryEntity);
		bookEntity = bookRepository.save(bookEntity);
		return bookConverter.toDto(bookEntity);
	}

	@Override
	public void delete(long[] ids) {
		for(long id : ids)
		{
			bookRepository.deleteById(id);
		}
	}
	
}
