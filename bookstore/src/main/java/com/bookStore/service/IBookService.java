package com.bookStore.service;

import com.bookStore.dto.BookDto;

public interface IBookService {

	BookDto save (BookDto bookDto);
	BookDto update (BookDto bookDto);
	void delete (long [] ids);
}
